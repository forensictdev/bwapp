FROM gitpod/workspace-mysql:branch-mysql

RUN rm -rf /app
USER root
RUN sed -i "s/allow_url_include = Off/allow_url_include = On/g" '/etc/php/7.2/cli/php.ini'
RUN sed -i "s/allow_url_include = Off/allow_url_include = On/g" '/etc/php/7.2/apache2/php.ini'

COPY /app /app/
ENV APACHE_DOCROOT_IN_REPO="app"
